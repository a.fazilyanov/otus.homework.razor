﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Services;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.Core.Services
{
    public class DateTimeService
        : IDateTimeService
    {
        public DateTime CurrentDateTime() => DateTime.Now;

        public string DataFormat(DateTime data) => data.ToString("dd.MM.yyyy hh:mm:ss");
        public string DataFormat(DateTime? data) => data?.ToString("dd.MM.yyyy hh:mm:ss");
    }
}