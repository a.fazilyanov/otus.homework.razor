﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Interfaces.Services
{
    public interface IDateTimeService
    {
        DateTime CurrentDateTime();

        string DataFormat(DateTime data);
        string DataFormat(DateTime? data);
    }
}