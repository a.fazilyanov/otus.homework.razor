﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.Mvc.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Mvc.Controllers
{
    public class PromoCodesController : Controller
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public PromoCodesController(IRepository<PromoCode> promoCodesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
        }

        public async Task<IActionResult> Index()
        {
            var promoCodes= await _promoCodesRepository.GetAllAsync();
            return View(promoCodes.Select(x => new PromoCodeViewModel(x)).ToList());
        }
        
        public async Task<IActionResult> Details(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
            {
                return NotFound();
            }

            var response = new PromoCodeViewModel(promoCode);

            return View(response);
        }

        public IActionResult Create()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PromoCodeViewModel request)
        {
            if (!ModelState.IsValid) 
                return View(request);

            var promoCode = new PromoCode()
            {
                ServiceInfo = request.ServiceInfo,
                BeginDate = request.BeginDate,
                Code = request.Code,
                EndDate = request.EndDate,
                PartnerName = request.PartnerName,

                PartnerManagerId = request.PartnerManager,
                PreferenceId = request.Preference
            };


            await _promoCodesRepository.AddAsync(promoCode);

            return RedirectToAction(nameof(Details), new { id = promoCode.Id });
        }
        
        public async Task<IActionResult> Edit(Guid id)
        {

            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
            {
                return NotFound();
            }

            return View(new PromoCodeViewModel(promoCode));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PromoCodeViewModel request)
        {
            if (!ModelState.IsValid) 
                return View(request);

            var promoCode = await _promoCodesRepository.GetByIdAsync(request.Id);

            if (promoCode == null)
                return NotFound();

            promoCode.ServiceInfo = request.ServiceInfo;
            promoCode.BeginDate = request.BeginDate;
            promoCode.Code = request.Code;
            promoCode.EndDate = request.EndDate;
            promoCode.PartnerManagerId = request.PartnerManager;
            promoCode.PartnerName = request.PartnerName;
            promoCode.PreferenceId = request.Preference;

            await _promoCodesRepository.UpdateAsync(promoCode);

            return RedirectToAction(nameof(Details), new { id = promoCode.Id });
        }
        
        public async Task<IActionResult> ConfirmDelete(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
            {
                return NotFound();
            }

            return View("Delete", new PromoCodeViewModel(promoCode));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
                return NotFound();

            await _promoCodesRepository.DeleteAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }
    }
}