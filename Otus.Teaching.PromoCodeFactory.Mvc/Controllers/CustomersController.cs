﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.Mvc.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Mvc.Controllers
{
    public class CustomersController : Controller
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomersController(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<IActionResult> Index()
        {
            var customers  =  await _customerRepository.GetAllAsync();
            return View(customers.Select(x => new CustomerShortViewModel(x)));
        }
        
        public async Task<IActionResult> Details(Guid id)
        {
            
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }
            
            return View(new CustomerViewModel(customer));
        }
        
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CustomerShortViewModel request)
        {
            if (!ModelState.IsValid) 
                return View(request);

            var customer = new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };

            await _customerRepository.AddAsync(customer);

            return RedirectToAction(nameof(Details), new {id = customer.Id});
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            return View(new CustomerShortViewModel(customer));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CustomerShortViewModel request)
        {
            if (!ModelState.IsValid) 
                return View(request);

            var customer = await _customerRepository.GetByIdAsync(request.Id);

            if (customer == null)
                return NotFound();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            
            await _customerRepository.UpdateAsync(customer);

            return RedirectToAction(nameof(Details), new { id= request.Id });
        }
        
        public async Task<IActionResult> ConfirmDelete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            return View("Delete", new CustomerShortViewModel(customer));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return RedirectToAction(nameof(Index));
        }

    }
}