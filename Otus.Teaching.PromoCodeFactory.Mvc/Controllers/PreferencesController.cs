﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.Mvc.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Mvc.Controllers
{
    public class PreferencesController : Controller
    {
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferencesController(IRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }

        public async Task<IActionResult> Index()
        {
            var preferences =  await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceViewModel(x)).ToList();

            return View(response);
        }
        

        public async Task<IActionResult> Details(Guid id)
        {

            var preference = await _preferencesRepository.GetByIdAsync(id);

            if (preference == null)
            {
                return NotFound();
            }

            return View(new PreferenceViewModel(preference));
        }

        
        public IActionResult Create()
        {
            return View();
        }

        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PreferenceViewModel request)
        {
            if (!ModelState.IsValid) 
                return View(request);

            var preference = new Preference()
            {
                Name = request.Name
            };

            await _preferencesRepository.AddAsync(preference);

            return RedirectToAction(nameof(Details), new { id = preference.Id });
        }
        
        public async Task<IActionResult> Edit(Guid id)
        {

            var preference = await _preferencesRepository.GetByIdAsync(id);

            if (preference == null)
            {
                return NotFound();
            }

            return View(new PreferenceViewModel(preference));
        }

        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PreferenceViewModel request)
        {
            if (!ModelState.IsValid) 
                return View(request);

            var preference = await _preferencesRepository.GetByIdAsync(request.Id);

            if (preference == null)
                return NotFound();

            preference.Name = request.Name;

            await _preferencesRepository.UpdateAsync(preference);

            return RedirectToAction(nameof(Details), new { id = request.Id });
        }


        public async Task<IActionResult> ConfirmDelete(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);

            if (preference == null)
            {
                return NotFound();
            }

            return View("Delete", new PreferenceViewModel(preference));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid Id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(Id);

            if (preference == null)
                return NotFound();

            await _preferencesRepository.DeleteAsync(preference);

            return RedirectToAction(nameof(Index));
        }
        
    }
}