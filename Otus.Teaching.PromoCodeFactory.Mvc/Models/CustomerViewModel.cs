﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Mvc.Models
{
    public class CustomerViewModel
    { 
        public CustomerViewModel()
        {
        }

        public CustomerViewModel(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;

            Preferences = customer.Preferences.Select(s => s.Preference.Name).ToList();
        }

       

        [Display(Name = "#")]
        public Guid Id { get; set; }

        [Display(Name = "Имя")] 
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")] 
        public string LastName { get; set; }

        [Display(Name = "Email")] 
        public string Email { get; set; }

        [Display(Name = "Предпочтения")] 
        public List<string> Preferences { get; set; }
    }
}