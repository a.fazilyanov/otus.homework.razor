﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Mvc.Models
{
    public class PromoCodeViewModel
    {

        public PromoCodeViewModel()
        {
            
        }

        public PromoCodeViewModel(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = promoCode.BeginDate;
            EndDate = promoCode.EndDate;
            PartnerName = promoCode.PartnerName;
            PartnerManager = promoCode.PartnerManagerId;
            Preference = promoCode.PreferenceId;
        }

        [Display(Name = "#")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 5)]
        [Display(Name = "Промокод")]
        public string Code { get; set; }

        [StringLength(50, MinimumLength = 5)]
        [Display(Name = "Информация")]
        public string ServiceInfo { get; set; }

        [Required]
        [Display(Name = "Дата начала")]
        public DateTime BeginDate { get; set; }

        [Required]
        [Display(Name = "Дата окончания")]
        public DateTime EndDate { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        [Display(Name = "Имя партнера")]
        public string PartnerName { get; set; }

        [Required]
        [Display(Name = "Guid менеджера")]
        public Guid PartnerManager { get; set; }

        [Required]
        [Display(Name = "Guid предпочтения")]
        public Guid Preference { get; set; }
    }
}