﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Mvc.Models
{
    public class PreferenceViewModel
    {
        public PreferenceViewModel()
        {
            
        }
 
        public PreferenceViewModel(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }

        [Display(Name = "#")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        [Display(Name = "Название")]
        public string Name { get; set; }
    }
}